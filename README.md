# 基于Node.js的即时通讯系统

### 一、项目简介

基于WebSocket网络通信协议，以Node.js为基础环境开发的一个B/S（浏览器/服务器）架构的即时通讯系统，实现在私聊和群聊两种场景下，用户间文本、表情、图片和文件信息的传输。同时结合了注册登录，用户身份认证，用户信息自定义，头像上传、用户、群聊关键词搜索，好友列表，好友添加和删除，消息提醒，群聊创建、加入和退出等基础功能。

#### 项目技术栈

浏览器端：vue + ElementUI + axios

服务器端：Egg + Sequelize + MySQL

#### 目录介绍

instant-messenger-client  前端项目

instant-messenger  前后端合并项目

### 二、启动项目

1. 启动项目之前，先按下面第三节的内容，在mysql数据库中创建imdb数据库(只需创建数据库即可，项目启动时，会自动创建各种表)。
2. 在instant-messenger目录中使用`npm install`下载依赖，然后执行`npm run dev`即可启动项目（启动前请先创建数据库，下面内容有介绍）
3. 浏览器访问 http://127.0.0.1:7001/index.html 即可打开项目

*在instant-messenger-client目录中使用`npm install`下载依赖，然后执行`npm run serve`即可启动前端项目*


### 三、数据库初始化

``` javascript
//连接数据库
config.sequelize={
  dialect:'mysql',
  database:'imdb',//数据库名称，需要自己创建
  host:"localhost",
  port:3306,
  username:'root',
  password:'111111',
  timezone:'+08:00',
};
```
### 四、项目展示
![](img/7.jpg)
![](img/8.jpg)
![](img/1.jpg)
![](img/2.jpg)
![](img/3.jpg)
![](img/9.jpg)
![](img/10.jpg)
![](img/11.jpg)
![](img/4.jpg)
![](img/5.jpg)
![](img/6.jpg)
