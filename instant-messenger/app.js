module.exports = app => {
    //beforeStart 是egg的生命周期函数，启动应用是执行。  
    app.beforeStart(async function() {
        // 应用会等待这个函数执行完成才启动
        // await app.model.sync({ force: true }); // 开发环境使用，每次重启会清空数据表，然后重新创建。
        await app.model.sync({});//初始化数据库，sync方法会根据模型（model文件夹下）去创建表
    });
};