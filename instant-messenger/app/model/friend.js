module.exports = app => {
    const { STRING,INTEGER,DATE } = app.Sequelize; 

    const Friend = app.model.define('friend',{
        user_id:INTEGER,    //用户id
        friend_id:INTEGER,  //好友id
        name:STRING,         //好友昵称
        state:INTEGER,      //好友状态（0为好友，1为申请中，2为申请发送方）
        tip:INTEGER,         //未读消息数
        time:DATE,         //生成时间
        lasttime:DATE      //最后通信时间
    })
    Friend.associate = function() {  
        // app.model.Friend.belongsTo(app.model.User,{
        //     foreignKey: 'user_id',
        //     targetKey: 'id',
        //     as:'user'
        // });
        app.model.Friend.belongsTo(app.model.User,{
            foreignKey: 'friend_id',
            targetKey: 'id',
            as:'user'
        });
    }
    return Friend;

}