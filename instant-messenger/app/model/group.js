module.exports = app => {
    const { STRING,INTEGER,DATE } = app.Sequelize; 

    const Group = app.model.define('group',{
        id: { type: INTEGER, primaryKey: true, autoIncrement: true },
        user_id:INTEGER,    //群主id
        name:STRING,        //群名
        imgurl:STRING,      //群头像
        time:DATE ,         //创建时间
        notice:STRING,      //公告
    })
    Group.associate = function() {  
        app.model.Group.belongsTo(app.model.User,{
            foreignKey: 'user_id',
            targetKey: 'id',
            as:'user'
        })
    }
    return Group;
}