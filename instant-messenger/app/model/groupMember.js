module.exports = app => {
    const { STRING,INTEGER,DATE } = app.Sequelize; 

    const GroupMember = app.model.define('groupMember',{
        group_id:INTEGER,   //群id
        user_id:INTEGER,    //用户id
        name:STRING,        //群内名称
        tip:INTEGER,        //未读消息数
        time:DATE ,         //加入时间
        lasttime:DATE,      //最后通信时间
        shield:INTEGER,     //是否频闭群消息（0不屏蔽，1频闭）
    })
    GroupMember.associate = function() {  
        app.model.GroupMember.belongsTo(app.model.Group,{
            foreignKey: 'group_id',
            targetKey: 'id',
            as:'user'
        });
    }
    return GroupMember;
}