module.exports = app => {
    const { STRING,DATE,INTEGER } = app.Sequelize; 
    
    const User = app.model.define('user',{
        id: { type: INTEGER, primaryKey: true, autoIncrement: true },
        name:STRING,      //用户名
        psw:STRING,       //密码
        email:STRING,     //邮箱
        sex:STRING,       //性别
        birth:DATE,       //生日
        explain:STRING,   //介绍
        imgurl:STRING,    //用户头像链接
        time:DATE,        //注册时间
    })

    return User
}