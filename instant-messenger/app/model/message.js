module.exports = app => {
    const { STRING,INTEGER,DATE } = app.Sequelize; 

    const Message = app.model.define('message',{
        user_id:INTEGER,    //用户id
        friend_id:INTEGER,  //好友id
        message:STRING,    //消息内容
        types:STRING,      //消息类型
        time:DATE,         //发送时间
        state:INTEGER      //接收状态（0已读，1未读）
    })
    Message.associate = function() {  
        app.model.Message.belongsTo(app.model.User,{
            foreignKey: 'user_id',
            targetKey: 'id',
            as:'user'
        })
    }
    return Message;
}