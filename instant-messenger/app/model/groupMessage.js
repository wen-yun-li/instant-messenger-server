module.exports = app => {
    const { STRING,INTEGER,DATE } = app.Sequelize; 

    const GroupMessage = app.model.define('groupMessage',{
        group_id:INTEGER,     //群ID
        user_id:INTEGER,      //用户ID
        message:STRING,       //消息内容
        types:STRING ,        //消息类型（0文字，1图片链接，2音频链接...）
        time:DATE,            //发送时间
    })
    GroupMessage.associate = function() {  
        app.model.GroupMessage.belongsTo(app.model.User,{
            foreignKey: 'user_id',
            targetKey: 'id',
            as:'user'
        });
    }
    return GroupMessage;
}