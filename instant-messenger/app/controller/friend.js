'use strict';

const Controller = require('egg').Controller;

class FriendController extends Controller {
    //1. 关键词搜索用户和群聊
    async search(){
        let keyword=this.ctx.request.body.keyword; //接收前端搜索关键词
        let searchResult= await this.ctx.service.friend.search(keyword);//搜索关键词用户
        this.ctx.body = searchResult
    }

    //2. 获取好友详情并判断好友状态
    async friendDetail(){
        let user_id = this.ctx.cookies.get('userid',{
            maxAge: 10000 * 1000,
            httpOnly: false,
            overwrite: true,
            signed: false,
        });
        let friend_id=this.ctx.params.id;//获取好友id
        let friendDetail=await this.ctx.service.friend.friendDetail(user_id,friend_id);//判断好友类型
        this.ctx.body=friendDetail;
    }

    //3. 好友昵称修改
    async changeFriendName(){
        let data = this.ctx.request.body.changeFriendNameData;
        let user_id=data.user_id;
        let friend_id=data.friend_id;
        let name=data.name;
        await this.ctx.service.friend.changeFriendName(user_id,friend_id,name);
        this.ctx.body = "昵称修改成功！";
    }

    //4. 好友申请
    async applyFriend(){
        let user_id = this.ctx.cookies.get('userid',{
            maxAge: 10000 * 1000,
            httpOnly: false,
            overwrite: true,
            signed: false,
        })
        let friend_id=this.ctx.request.body.friend_id;
        let msg=this.ctx.request.body.msg;
        let nickName=this.ctx.request.body.nickName;
        let applyResult=await this.ctx.service.friend.applyFriend(user_id,friend_id,msg,nickName);
        if(applyResult){
            this.ctx.body="好友申请已发送!"
        }else{
            this.ctx.body="申请失败！"
        }
    }

    //5. 同意好友申请(更新好友状态state)
    async updateFriendState(){
        let user_id = this.ctx.cookies.get('userid',{
            maxAge: 10000 * 1000,
            httpOnly: false,
            overwrite: true,
            signed: false,
        })
        let friend_id=this.ctx.request.body.friend_id;
        let nickName=this.ctx.request.body.nickName;
        let updateResult=await this.ctx.service.friend.updateFriendState(user_id,friend_id,nickName);
        if(updateResult){
             this.ctx.body="好友状体更新成功，已同意好友申请！"
        }else{
            this.ctx.body="好友状态更新失败！"
        }
       
    }

    //6. 拒绝/删除好友
    async deleteFriend(){
        let user_id = this.ctx.cookies.get('userid',{
            maxAge: 10000 * 1000,
            httpOnly: false,
            overwrite: true,
            signed: false,
        })
        let friend_id=this.ctx.request.body.friend_id;
        let deleteResult=await this.ctx.service.friend.deleteFriend(user_id,friend_id);
        if(deleteResult){
           this.ctx.body="删除好友/拒绝好友申请成功！" 
        }else{
            this.ctx.body="删除好友/拒绝好友申请失败！"
        }
        
    }

    //7.获取全部好友
    async getFriendList(){
       let id = this.ctx.cookies.get('userid',{
            maxAge: 10000 * 1000,
            httpOnly: false,
            overwrite: true,
            signed: false,
        })
        let result=await this.ctx.service.friend.getFriendList(id);
        this.ctx.body=result;
    }

}

module.exports = FriendController;