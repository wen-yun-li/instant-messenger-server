'use strict';

const Controller = require('egg').Controller;

class MessageController extends Controller {
  //1. 获取消息列表
  async index() {
    //从cookie中获取user_id
    let user_id = this.ctx.cookies.get('userid',{
      httpOnly: false,
      overwrite: true,
      signed: false,});

    let messageList=[];
    //查找好友聊天列表
    const friendList=await this.app.model.Friend.findAndCountAll({
      'order':[['lasttime','DESC']],
      attributes:["lasttime","name","tip"], //最后通信时间、好友昵称
      include:[{attributes:["id","name","imgurl"],model:this.app.model.User,as:'user'}],
      where:{user_id:user_id}
    });
    let friendMessageList=friendList.rows;
    
    //查找群聊聊天列表
    const groupList=await this.app.model.GroupMember.findAndCountAll({
      'order':[['lasttime','DESC']],
      attributes:["lasttime","tip","name","shield"], //最后通话时间、未读消息数、自己在群内的昵称、时候屏蔽群消息（0不屏蔽，1屏蔽）
      include:[{attributes:["id","name","imgurl"],model:this.app.model.Group,as:'user'}],
      where:{user_id:user_id}
    });
    let groupMessageList=groupList.rows;
   
    //将两个列表合并并按最后通信时间重新排序
    messageList.push(...friendMessageList)
    messageList.push(...groupMessageList)
    let messageLists=messageList.sort(this.compare("lasttime"));
    this.ctx.body = messageLists ;
  }

   //排序函数
   compare(property){
    return function(obj1,obj2){
        var value1 = obj1[property];
        var value2 = obj2[property];
        return value2 - value1;     // 降序
       }
   }

   //2. 获取好友聊天信息
   async getFriendMessageList(){
    let user_id = this.ctx.cookies.get('userid',{
      httpOnly: false,
      overwrite: true,
      signed: false,});
    let friend_id=this.ctx.params.id;
    //查找与该好友的所有聊天记录
    let friendMessageList=await this.app.model.Message.findAll({
      attributes:["user_id","friend_id","message","types","time","state"],
      include:[{attributes:["id","name","imgurl"],model:this.app.model.User,as:'user'}],
      where:{$or:[{user_id:user_id,friend_id:friend_id},{user_id:friend_id,friend_id:user_id}]}
    });
    //将未读消息数清零
    await this.app.model.Friend.update({tip:0},{where:{user_id:user_id,friend_id:friend_id}});
    this.ctx.body=friendMessageList;
   }
  
   //去除未读消息数清零，用于发送和接收消息更新消息记录
   async getFriendMessageList2(){
    let user_id = this.ctx.cookies.get('userid',{
      httpOnly: false,
      overwrite: true,
      signed: false,});
    let friend_id=this.ctx.params.id;
    let friendMessageList=await this.app.model.Message.findAll({
      attributes:["user_id","friend_id","message","types","time","state"],
      include:[{attributes:["id","name","imgurl"],model:this.app.model.User,as:'user'}],
      where:{$or:[{user_id:user_id,friend_id:friend_id},{user_id:friend_id,friend_id:user_id}]}
    });
    this.ctx.body=friendMessageList;
   }
   
   //3. 获取群聊聊天信息
   async getGroupMessageList(){ 
    let user_id = this.ctx.cookies.get('userid',{
      httpOnly: false,
      overwrite: true,
      signed: false,}); 
    let group_id=this.ctx.params.id;
    let groupMessageList=await this.app.model.GroupMessage.findAll({
      attributes:["group_id","user_id","message","types","time"], //最后通话时间、未读消息数、自己在群内的昵称、时候屏蔽群消息（0不屏蔽，1屏蔽）
      include:[{attributes:["id","name","imgurl"],model:this.app.model.User,as:'user'}],
        where:{group_id:group_id}
      });
    //将未读消息数清零
    await this.app.model.GroupMember.update({tip:0},{where:{user_id:user_id,group_id:group_id}});
    this.ctx.body=groupMessageList;
   };

 //3. 获取群聊聊天信息
 async getGroupMessageList2(){ 
  // let user_id = this.ctx.cookies.get('userid',{
  //   httpOnly: false,
  //   overwrite: true,
  //   signed: false,}); 
  let group_id=this.ctx.params.id;
  let groupMessageList=await this.app.model.GroupMessage.findAll({
    attributes:["group_id","user_id","message","types","time"], //最后通话时间、未读消息数、自己在群内的昵称、时候屏蔽群消息（0不屏蔽，1屏蔽）
    include:[{attributes:["id","name","imgurl"],model:this.app.model.User,as:'user'}],
      where:{group_id:group_id}
    });
  this.ctx.body=groupMessageList;
 };

   //4. 从好友详情界面跳转到消息发送界面（更新最后通信时间）
   async toFriendMessageList(){
      let user_id = this.ctx.cookies.get('userid',{
        httpOnly: false,
        overwrite: true,
        signed: false,}); 
      let friend_id=this.ctx.params.id;
      await this.app.model.Friend.update({
        lasttime:new Date()
      },{
        where:{user_id:user_id,friend_id:friend_id}
      });
      this.ctx.body= true;
   }

   //5. 从群聊详情界面跳转到消息发送界面（更新最后通信时间）
   async toGroupMessageList(){
    let user_id = this.ctx.cookies.get('userid',{
      httpOnly: false,
      overwrite: true,
      signed: false,}); 
    let group_id=this.ctx.params.id;
    await this.app.model.GroupMember.update({
      lasttime:new Date()
    },{
      where:{user_id:user_id,group_id:group_id}
    });

    this.ctx.body= true;
 }
  



}

module.exports = MessageController;
