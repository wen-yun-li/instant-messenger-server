'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller,io } = app;
  router.get('/messageList',app.middleware.checktoken(),controller.message.index); //首页
  router.get('/getFriendMessageList/:id',app.middleware.checktoken(),controller.message.getFriendMessageList);//获取好友聊天信息
  router.get('/getFriendMessageList2/:id',app.middleware.checktoken(),controller.message.getFriendMessageList2);
  router.get('/getGroupMessageList2/:id',app.middleware.checktoken(),controller.message.getGroupMessageList2);
  router.get('/getGroupMessageList/:id',app.middleware.checktoken(),controller.message.getGroupMessageList);//获取群聊聊天信息
  router.get('/toFriendMessageList/:id',app.middleware.checktoken(),controller.message.toFriendMessageList);//从好友详情页跳转到消息界面
  router.get('/toGroupMessageList/:id',app.middleware.checktoken(),controller.message.toGroupMessageList);//从群聊详情页跳转到消息界面
  router.post('/register',controller.user.register); //注册
  router.post('/login',controller.user.login); //登录
  router.post('/user/search',app.middleware.checktoken(),controller.friend.search); //用户关键词搜索
  router.get('/user/detail',app.middleware.checktoken(),controller.user.userDetail); //用户详情获取
  router.get('/user/detail/:id',app.middleware.checktoken(),controller.user.userDetailId)//路由传参获取用户详情
  router.post('/user/detail/change',app.middleware.checktoken(),controller.user.userDetailChange); //用户信息修改
  router.post('/user/psw/change',app.middleware.checktoken(),controller.user.changePassword); //用户密码修改
  router.get('/group/groupList',app.middleware.checktoken(),controller.group.getGroupList);//获取群聊列表
  router.get('/group/groupMembers/:id',app.middleware.checktoken(),controller.group.getGroupMembers);//获取群成员列表
  router.post('/group/search',app.middleware.checktoken(),controller.group.groupSearch);  //群聊关键词搜索
  router.get('/group/groupDetail/:id',app.middleware.checktoken(),controller.group.groupDetail);  //获取群聊信息并判断自己在不在群里
  router.post('/group/createGroup',app.middleware.checktoken(),controller.group.createGroup);//建群
  router.post('/group/applyGroup',app.middleware.checktoken(),controller.group.applyGroup);//加入群聊
  router.get('/group/deleteGroup/:group_id',app.middleware.checktoken(),controller.group.deleteGroup);//退出群聊
  router.get('/friend/friendList',app.middleware.checktoken(),controller.friend.getFriendList);//获取好友列表
  router.get('/friend/friendDetail/:id',app.middleware.checktoken(),controller.friend.friendDetail); //好友详情并判断好友状态
  router.post('/friend/applyFriend',app.middleware.checktoken(),controller.friend.applyFriend);  //好友申请
  router.post('/friend/changeFriendName',app.middleware.checktoken(),controller.friend.changeFriendName);//修改好友昵称
  router.post('/friend/updateFriendState',app.middleware.checktoken(),controller.friend.updateFriendState);// 同意好友申请
  router.post('/friend/deleteFriend',app.middleware.checktoken(),controller.friend.deleteFriend);//删除/拒绝好友
  router.post('/user/uploadAvatar',app.middleware.checktoken(),controller.upload.userAvatarUpload);//用户头像上传
  router.post('/group/uploadAvatar',app.middleware.checktoken(),controller.upload.groupAvatarUpload);//群头像上传

  io.of('/').route('sendFriendMessage', io.controller.chatController.sendFriendMessage);//私聊
  io.of('/').route('sendGroupMessage',io.controller.chatController.sendGroupMessage);//群聊
  io.of('/').route('login', io.controller.chatController.login);
  io.of('/').route('disconnect',io.controller.chatController.disconnect);

  router.post('/sendFriendImage',io.controller.chatController.sendFriendImage);
  router.post('/sendGroupImage',io.controller.chatController.sendGroupImage);
  router.post('/sendFriendFile',io.controller.chatController.sendFriendFile);
  router.post('/sendGroupFile',io.controller.chatController.sendGroupFile);

  // io.of('/').route('sendFriendImage',io.controller.chatController.sendFriendImage);
  // io.of('/').route('sendGroupImage',io.controller.chatController.sendGroupImage);
  // io.of('/').route('sendFriendFile',io.controller.chatController.sendFriendFile);
  // io.of('/').route('sendGroupFile',io.controller.chatController.sendGroupFile)
};
