'use strict';

const Service = require('egg').Service;
const nodemailer=require('nodemailer');
const bcrypt=require('bcryptjs');

class UserService extends Service {

    //1. 邮件发送
    async mailSet(username,mail) {
            // 使用默认 SMTP 传输创建可重用传输器对象
            let transporter = nodemailer.createTransport({
                host: 'smtp.qq.com',    //QQ邮箱SMTP服务器主机名
                port: 465,              //端口号
                secure: true, // true for 465, false for other ports
                auth: {
                    user: '3296657215@qq.com', // generated ethereal user
                    pass: 'rhzusbciesmmdadh'  // 授权码
                }
            });
            
            // 使用 unicode 符号设置电子邮件数据
            let mailOptions = {
                from: '3296657215@qq.com', // sender address
                to:`${mail}`, // list of receivers
                subject: `${username}，你好！` , // Subject line
                text: '基于Node.js的即时通讯系统', // plain text body
                html: '<h3>欢迎您的加入!</h3><p>此项目为暨南大学智能科学与工程学院电子信息科学与技术专业2022届本科毕业生李文运同学的毕业设计——基于Node.js的即时通讯系统。</p>' // html body
            };
        
            // 使用定义的传输对象发送邮件
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    return;
                }
                console.log("邮件发送成功！",info.response);
            });
      }

      //2. 密码加密
      async encryption(e){
          //生成随机的salt
          let salt=bcrypt.genSaltSync(10);
          //生成hash密码
          let hash=bcrypt.hashSync(e,salt);
          return hash;
      }

      //3. 密码匹配
      async verification(e,hash){
          let verif=bcrypt.compareSync(e,hash);//如果匹配成功，返回true
          return verif;
      }

      //4. 存储用户注册信息
      async buildUser(username,psw,mail){
          //对密码进行加密
          let password= await this.encryption(psw);
          try {
            //将注册信息存储到user表
             let user={
                  name:username,
                  email:mail,
                  psw:password,
                  time:new Date(),
                };
                await this.app.model.User.create(user);
                return true; 
            } catch (error) {
                return false;
            }
      }

      //5.验证注册信息是否存在
      async verifyuser(mail){
        const user = await this.app.model.User.findOne({
            where: {
                email:mail
            }
        });
        if(user){
            return false;
        }else{
            return true;   
        }
      }
      
      //6.登录验证
      async login(mail,psw){
        //根据用户登录时所填的邮箱检索数据库中用户的完整信息
        const user = await this.app.model.User.findOne({
            where: {
                email:mail
            }
        });

        if(user){
            //检索出登录用户存储于数据库中的密码，与登录时所填的密码进行验证
            let password=user.psw;
            let result=this.verification(psw,password);
            let user_id = user.id;
            //密码校验通过后，利用用户的邮箱号生成token,并将user_id存储于cookie中
            if(result){  
                this.ctx.cookies.set('userid', user_id, {
                    maxAge: 10000 * 1000,
                    httpOnly: false,
                    overwrite: true,
                    signed: false,
                });

                const token=this.app.jwt.sign({
                    email:mail
                },this.app.config.jwt.secret);

                return token  
            }else{
                return false;
            };
           
        }else{
            return false;
        }
      }

      //7. 用户主页详情
      async userDetail(id){
      let detailData=await this.app.model.User.findOne({where:{id:id}})
      if(detailData){
          return detailData;
        }else{
            return{
                code:400,
                message:'用户信息获取失败！'
            }
        }
      }
    
      //8. 用户信息修改
      async userDetailChange(id,changeDetailData){
          if(changeDetailData.name&&changeDetailData.sex&&changeDetailData.explain&&changeDetailData.birth){
            await this.app.model.User.update({
                name:changeDetailData.name,
                sex:changeDetailData.sex,
                explain:changeDetailData.explain,
                birth:changeDetailData.birth
            },{where:{id:id}});
            return {
                code:200,
                message:'信息更新成功！'
            }
          }else{
              return {
                  code:500,
                  message:'信息不完整，请完善信息！'
              }
          }
      }

      //9. 用户密码修改
      async changePassword(id,passwordData){
          let userData=await this.app.model.User.findOne({where:{id:id}})
          let oldPassword=userData.psw;
          let inputOldPassworld=passwordData.oldPassword;
          let inputNewPassword=passwordData.newPassword;
          let inputNewPassword2=passwordData.newPassword2;
          let judgeResult=await this.verification(inputOldPassworld,oldPassword);

          if(judgeResult){
              if(inputNewPassword==inputOldPassworld){
                  return{
                      code:500,
                      message:'新密码和旧密码重复！'
                  }
              }else{
                 if(inputNewPassword==inputNewPassword2){
                  let encrypInputNewPassword=await this.encryption(inputNewPassword)
                  await this.app.model.User.update({
                      psw:encrypInputNewPassword
                  },{where:{id:id}});
                  return {
                      code:200,
                      message:'密码修改成功！'
                  }
              }else{
                  return{
                      code:500,
                      message:"两次输入的新密码不匹配！"
                  }
              }  
            }   
          }else{
              return{
                  code:500,
                  message:"原密码错误！"
              }
          }

      }



}

module.exports = UserService;