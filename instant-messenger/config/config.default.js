/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
const path=require('path');
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1647309438662_3038';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

 //设置跨域请求
  config.cors={
    credentials:true,    
    origin:"http://127.0.0.1:8080",
    allowMethods:'GET,HEAD,PUT,POST,DELETE,PATCH'
  };
  
 //允许跨域访问,关闭csrf认证
  config.security = {
    csrf: {
        enable: false, // 前后端分离，post请求不方便携带_csrf
    },
    domainWhiteList: [
        'http://127.0.0.1:8080',
    ], //配置白名单
};
const Op=require('sequelize').Op;
//连接数据库
config.sequelize={
  dialect:'mysql',
  database:'imdb',
  host:"localhost",
  port:3306,
  username:'root',
  password:'111111',
  charset:'utf8mb4',
  timezone:'+08:00',
  operatorsAliases:{
    $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col
  }
};


//设置token密钥
config.jwt={
  secret:"liwenyun"
};

config.multipart = {
  mode: 'file',
  fileExtensions:['.docx','.doc','.pdf','.mp4','.txt']
};

config.io = {
  // init: {}, // passed to engine.io
  namespace: {
    '/': {
      connectionMiddleware: [],//处理连接等中间件
      packetMiddleware: [],//数据加密等中间件
    },
  },

  // redis: {
  //   host: '127.0.0.1',
  //   port: 6379,
  //   password: '',
  //   db: 0,
  // },
};

config.static={
    prefix:'/',
    dir:path.join(appInfo.baseDir,'app/public')
}
  return {
    ...config,
    ...userConfig,
  };
};
