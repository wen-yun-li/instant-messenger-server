//封装请求方法，每次请求就没必要写完整的请求路径了

import axios from 'axios'
const request = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, //VUE_APP_BASE_API可以在env.development(开发环境）和env.production（生产环境）中自定义。
    timeout: 5000,
    withCredentials:true,//允许携带cookie
    crossDomain: true,
})

//拦截器，实现每次请求都携带token
request.interceptors.request.use(
    req => {
        if (localStorage.getItem("token")) {
            req.headers['token'] = localStorage.getItem("token")
        }
        return req
    }
)



export default request