import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import mavonEditor from 'mavon-editor'
// import 'mavon-editor/dist/css/index.css'
import VueSocketIO from 'vue-socket.io'

Vue.use(mavonEditor)
Vue.config.productionTip = false
Vue.use(ElementUI);

Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://127.0.0.1:7001',
  // options:{autoConnect:false},//设置其不要自动连接
}))


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
